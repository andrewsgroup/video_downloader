import urllib.error

import pytube
from pytube import YouTube
video_url = input("Enter The Youtube URL Link Of The Video You Wish To Download:  ")
try:
    youtube = pytube.YouTube(video_url)
    video = youtube.streams.first()
    video.download('F:/download/')
    print("Video Downloaded Successfully")
except pytube.exceptions.RegexMatchError:
    print(f"URL => {video_url} is incorrect")
except urllib.error.URLError:
    print("You do not have an active internet connection")